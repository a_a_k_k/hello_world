#!/bin/bash

git_tag=$(git describe | cut -d '-' -f1)
echo "Current tag: $git_tag"
if [ -z $git_tag ]; then
    $git_tag="0.0"
fi

dir=$(git worktree list | awk '{print $1}')
name=$(grep "APP_NAME" .env | cut -d '=' -f2)
file="$dir/version"
tag=$(head -n1 $file)
if [ -z $tag ]; then
    echo "No tag in $file"
    exit
fi

echo "Tag in version file: $tag"
if [ $git_tag == $tag ]; then
    echo "Current git tag and tag in version file are same: $tag"
    exit
fi

if [ $git_tag != $tag ]; then
    echo "Change tag from $git_tag to $tag"
    commit_msg=$(git log -1 --pretty=%s)
    git tag -a $tag -m "$commit_msg"
fi

n=$(grep -n "APP_TAG" .env | cut -d ':' -f1)
sed -i ${n}d .env
echo "APP_TAG=$tag" >> .env
echo "Change APP_TAG in $dir/.env to $tag"
account=$(grep "ACCOUNT_NAME" .env | cut -d '=' -f2)

docker build -t $name:$tag .
docker tag $name:$tag $account/$name:$tag
docker push $account/$name:$tag
docker-compose up
