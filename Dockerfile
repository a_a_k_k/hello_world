FROM golang:alpine
WORKDIR /go/bin/
COPY src/ /go/src/
COPY .env .
RUN cd /go/src/ \
&& go build -o /go/bin/ \
&& mv index.html /go/bin/
EXPOSE 8080
CMD hello-world
