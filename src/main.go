package main

import (
    "fmt"
    "log"
    "net/http"
    "os"
    "github.com/joho/godotenv"
)

func main() {
    err := godotenv.Load(".env")
    if err != nil {
        log.Fatalf("Some error occured. Err: %s", err)
    }
    address := os.Getenv("APP_ADDRESS")
    port := ":" + os.Getenv("APP_PORT")
    name := os.Getenv("APP_NAME")
    tag := ":" + os.Getenv("APP_TAG")
    fmt.Println("Application name and version is", name + tag)
    fmt.Println("Server is running on", address + port)
    log.Fatal(http.ListenAndServe(address + port, http.FileServer(http.Dir("."))))
}
